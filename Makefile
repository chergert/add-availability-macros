all: add-availability-macros

PKGS = gio-2.0 json-glib-1.0

FILES = \
	add-availability-macros.c \
	ide-compile-commands.c

add-availability-macros: $(FILES)
	$(CC) -g -o $@.tmp -Wall -Werror $(shell pkg-config --cflags --libs $(PKGS)) $(FILES) -lclang
	mv $@.tmp $@

clean:
	rm -f add-availability-macros

