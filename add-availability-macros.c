/* add-availability-macros.c
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <clang-c/Index.h>
#include <gio/gio.h>
#include <glib/gi18n.h>
#include <stdlib.h>

#include "ide-compile-commands.h"

typedef struct
{
  gchar *filename;
  guint  line;
} EditLocation;

static GHashTable *headers;
static GHashTable *lines_by_filename;

static gboolean
is_in_header (CXCursor   cursor,
              gchar    **filename,
              unsigned  *out_line)
{
  CXSourceLocation loc = clang_getCursorLocation (cursor);
  g_autofree gchar *abspath = NULL;
  CXFile file;
  CXString cxname;
  const gchar *name;
  unsigned line, column, offset = 0;
  enum CXVisibilityKind vis;

  clang_getFileLocation (loc, &file, &line, &column, &offset);
  cxname = clang_getFileName (file);
  name = clang_getCString (cxname);
  vis = clang_getCursorVisibility (cursor);

  if (vis == CXVisibility_Protected || vis == CXVisibility_Hidden)
    goto failure;

  if (!g_path_is_absolute (name))
    {
      g_autoptr(GFile) file = g_file_new_for_commandline_arg (name);
      name = abspath = g_file_get_path (file);
    }

  if (!g_str_has_suffix (name, ".h"))
    goto failure;

  if (g_hash_table_contains (headers, name))
    *filename = g_strdup (name);
  else
    *filename = NULL;

  *out_line = line - 1;

  return !!*filename;

failure:
  *filename = NULL;
  *out_line = 0;
  return FALSE;
}

static enum CXChildVisitResult
find_edit_points (CXCursor     cursor,
                  CXCursor     parent,
                  CXClientData client_data)
{
  GArray *edit_points = client_data;
  enum CXCursorKind kind = clang_getCursorKind (cursor);

  if (kind == CXCursor_FunctionDecl)
    {
      CXCursor canon = clang_getCanonicalCursor (cursor);
      g_autofree gchar *path = NULL;
      CXString str;
      const gchar *cstr;
      gboolean ignore;
      unsigned line = 0;

      str = clang_getCursorSpelling (cursor);
      cstr = clang_getCString (str);
      ignore = cstr && (*cstr == '_');
      clang_disposeString (str);

      if (!ignore && is_in_header (canon, &path, &line))
        {
          EditLocation loc = { g_steal_pointer (&path), line };

          g_array_append_val (edit_points, loc);
        }
    }
  else
    {
      clang_visitChildren (cursor, find_edit_points, edit_points);
    }

  return CXChildVisit_Recurse;
}

static gint
compare_edit_points (const EditLocation *a,
                     const EditLocation *b)
{
  gint ret;

  ret = g_strcmp0 (a->filename, b->filename);
  if (ret == 0)
    ret = (gint)b->line - (gint)a->line;
  return ret;
}

gint
main (gint argc,
      gchar *argv[])
{
  g_autoptr(IdeCompileCommands) commands = ide_compile_commands_new ();
  g_autofree gchar *compile_commands_path = NULL;
  g_autoptr(GFile) compile_commands_file = NULL;
  g_autoptr(GOptionContext) context = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GArray) edit_points = g_array_new (FALSE, FALSE, sizeof (EditLocation));
  EditLocation last = { 0 };
  CXIndex idx;
  GOptionEntry entries[] = {
    { "compile-commands", 'c', 0, G_OPTION_ARG_FILENAME, &compile_commands_path,
      N_("The path to a compile_commands.json"), N_("PATH") },
    { NULL }
  };

  context = g_option_context_new ("— add availability macros");
  g_option_context_add_main_entries (context, entries, NULL);

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_printerr ("%s\n", error->message);
      return EXIT_FAILURE;
    }

  if (compile_commands_path != NULL)
    {
      compile_commands_file = g_file_new_for_commandline_arg (compile_commands_path);

      if (!ide_compile_commands_load (commands, compile_commands_file, NULL, &error))
        {
          g_printerr ("%s\n", error->message);
          return EXIT_FAILURE;
        }
    }

  lines_by_filename = g_hash_table_new_full (g_str_hash, g_str_equal, g_free,
                                             (GDestroyNotify)g_ptr_array_unref);

  /* Keep track of headers we need to process */
  headers = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, NULL);
  for (guint i = 1; i < argc; i++)
    {
      g_autoptr(GFile) file = g_file_new_for_commandline_arg (argv[i]);
      g_autofree gchar *path = g_file_get_path (file);
      g_hash_table_insert (headers, g_steal_pointer (&path), NULL);
    }

  idx = clang_createIndex (0, 0);

  for (guint i = 1; i < argc; i++)
    {
      g_autoptr(GFile) file = g_file_new_for_commandline_arg (argv[i]);
      g_autoptr(GFile) dir = NULL;
      g_auto(GStrv) file_args = ide_compile_commands_lookup (commands, file, &dir, NULL);

      if (file_args == NULL)
        {
          const gchar *rdot = strrchr (argv[i], '.');
          g_autofree gchar *prefix = g_strndup (argv[i], rdot - argv[i]);
          g_autofree gchar *c_file = g_strjoin ("", prefix, ".c", NULL);
          g_autoptr(GFile) alternate = g_file_new_for_commandline_arg (c_file);

          file_args = ide_compile_commands_lookup (commands, alternate, &dir, NULL);
        }

      if (file_args != NULL)
        {
          CXTranslationUnit unit;

          g_print ("Parsing (%d of %d) %s\n", i, argc, argv[i]);

          unit = clang_parseTranslationUnit (idx, argv[i],
                                             (const gchar * const *)file_args,
                                             g_strv_length (file_args),
                                             NULL, 0,
                                             (CXTranslationUnit_DetailedPreprocessingRecord |
                                              CXTranslationUnit_SkipFunctionBodies));
          clang_visitChildren (clang_getTranslationUnitCursor (unit),
                               find_edit_points,
                               edit_points);
          clang_disposeTranslationUnit (unit);
        }
    }

  g_array_sort (edit_points, (GCompareFunc)compare_edit_points);

  for (guint i = 0; i < edit_points->len; i++)
    {
      EditLocation *loc = &g_array_index (edit_points, EditLocation, i);
      GPtrArray *lines;

      if (g_strcmp0 (last.filename, loc->filename) == 0 && last.line == loc->line)
        continue;

      if (!(lines = g_hash_table_lookup (lines_by_filename, loc->filename)))
        {
          g_autofree gchar *contents = NULL;
          gsize len = 0;

          lines = g_ptr_array_new_with_free_func (g_free);
          g_hash_table_insert (lines_by_filename, g_strdup (loc->filename), lines);

          if (g_file_get_contents (loc->filename, &contents, &len, NULL))
            {
              g_autofree gchar **split = g_strsplit (contents, "\n", 0);
              for (guint j = 0; split[j]; j++)
                g_ptr_array_add (lines, split[j]);
              g_ptr_array_add (lines, NULL);
            }
        }

      if (loc->line < lines->len &&
          strstr (g_ptr_array_index (lines, loc->line), "G_DE") != NULL)
        continue;

      g_ptr_array_insert (lines, loc->line, g_strdup ("IDE_AVAILABLE_IN_ALL"));

      g_print ("%s (%u)\n", loc->filename, loc->line);

      last = *loc;
    }

  {
    GHashTableIter iter;
    gpointer key, value;

    g_hash_table_iter_init (&iter, lines_by_filename);

    while (g_hash_table_iter_next (&iter, &key, &value))
      {
        const gchar *path = key;
        GPtrArray *lines = value;
        g_autofree gchar *data = NULL;
        gboolean found_rel = FALSE;
        guint last_inc = 0;

        /* Add the header in */
        for (guint j = 0; j < lines->len; j++)
          {
            const gchar *str = g_ptr_array_index (lines, j);

            if (!str)
              break;

            if (g_str_has_prefix (str, "#include <"))
              last_inc = j;

            if (g_str_has_prefix (str, "#include \""))
              {
                last_inc = j;
                found_rel = TRUE;
                break;
              }
          }

        if (found_rel)
          {
            g_ptr_array_insert (lines, last_inc, g_strdup ("#include \"ide-version-macros.h\""));
            g_ptr_array_insert (lines, last_inc + 1, g_strdup (""));
          }
        else
          {
            g_ptr_array_insert (lines, last_inc + 1, g_strdup (""));
            g_ptr_array_insert (lines, last_inc + 2, g_strdup ("#include \"ide-version-macros.h\""));
          }

        data = g_strjoinv ("\n", (gchar **)lines->pdata);
        g_file_set_contents (path, data, -1, NULL);
      }
  }

  return EXIT_SUCCESS;
}
